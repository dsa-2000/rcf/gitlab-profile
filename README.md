# Radio Camera Frontend

# Repository Groups

1. [FSM](https://gitlab.com/dsa-2000/rcf/fsm): Hardware, firmware, and software for the RCF FPGA Station Module (FSM)
2. [docs](https://gitlab.com/dsa-2000/rcf/docs): RCF Documents

# Documents

The documents below are hosted in individual git repositories under `docs/`.
Links to the latest CI-built PDFs are provided here for convenience.

## Design Documents

1. DSA-2000 Doc 00016: [RCF Preliminary Design](https://gitlab.com/api/v4/projects/56928121/jobs/artifacts/main/raw/D2k-00016-RCF-DES-Preliminary_Design.pdf?job=build_job)


## Interface Control Documents

1. DSA-2000 Doc 00019: [RCF-RCP Interface Specification](https://gitlab.com/api/v4/projects/56928147/jobs/artifacts/main/raw/D2k-00019-RCF-ICD-RCF_RCP_interface.pdf?job=build_job)

## Test Reports

1. DSA-2000 Doc 00107: [AD9082 Synchronization Test Report](https://gitlab.com/api/v4/projects/57835303/jobs/artifacts/main/raw/DSA2k00107-RCF-AD9082-Synchronization-Test-Report.pdf?job=build_job)